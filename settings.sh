APP=redis

### Uncomment PORTS if you want this redis container to be accessed
### from the internet. In this case make sure that PASS is uncommented
### too.
#PORTS="6379:6379"

### Use something like `pwgen 32` to generate a strong password.
### Since Redis is pretty fast an outside user can try up to 150k
### passwords per second against a good box. This means that you
### should use a very strong password otherwise it will be very easy
### to break.
PASS='ohhebahQuahghaingeef1ifeitah5yei'
