#!/bin/bash -x

### make sure that directories have correct ownership
chown redis: -R conf/ data/ logs/

### install redis while keeping any existing config files unchanged
apt update && apt -y upgrade
export DEBIAN_FRONTEND=noninteractive
apt -y install redis-server \
    -o Dpkg::Options::="--force-confdef" \
    -o Dpkg::Options::="--force-confold"

### allow requests from the network
sed -i /etc/redis/redis.conf \
    -e 's/^bind/#bind/'

### fix redis service
mkdir -p /lib/systemd/system/redis-server.service.d
cat <<EOF > /lib/systemd/system/redis-server.service.d/override.conf
[Service]
PrivateUsers=false
ProtectHostname=false
EOF
systemctl daemon-reload
systemctl restart redis

source /host/settings.sh

### enable protected-mode if a password is given
if [[ -n $PASS ]]
then
    sed -i /etc/redis/redis.conf \
        -e '/^protected-mode/ c protected-mode yes' \
        -e "/^# requirepass/ c requirepass $PASS"
else
    sed -i /etc/redis/redis.conf \
        -e '/^protected-mode/ c protected-mode no'
fi
