rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p conf data logs
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/conf,dst=/etc/redis \
        --mount type=bind,src=$(pwd)/data,dst=/var/lib/redis \
        --mount type=bind,src=$(pwd)/logs,dst=/var/log/redis
}
