# Redis in a Container

## Installation

  - First install `ds`: https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull redis`

  - Create a directory for the container: `ds init redis @redis`

  - Edit the settings and change the PASS: `cd /var/ds/redis/ && vim settings.sh`

  - Build image, create the container and configure it: `ds make`
